import "./App.css";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import routes from "./route";
import { useDispatch } from "react-redux";
import { useEffect } from "react";
import { fetchData } from "./slices/contact-actions";

const router = createBrowserRouter(routes);

let initial = true;
const App = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    if (initial) {
      dispatch(fetchData());
      initial = false;
    }
  }, [dispatch]);
  return <RouterProvider router={router}></RouterProvider>;
};

export default App;
