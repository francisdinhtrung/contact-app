import DefaultLayout from "./layout/DefaultLayout";
import HomePage from "./pages/HomePage";
import ContactDetailPage from "./pages/ContactDetailPage";
import NotFoundPage from "./pages/NotFoundPage";
import LoginPage from "./pages/LoginPage";

const routes = [
  {
    path: "/",
    element: <DefaultLayout />,
    children: [
      {
        path: "/",
        element: <HomePage />,
      },
      {
        path: "/contact-detail/:userId",
        element: <ContactDetailPage />,
      },
      {
        path: "/login",
        element: <LoginPage />,
      },
      {
        path: "*",
        element: <NotFoundPage />,
      },
    ],
  },
];
export default routes;
