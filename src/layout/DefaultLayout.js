import AppHeader from "../components/AppHeader";
import AppContent from "../components/AppContent";
const DefaultLayout = () => {
  return (
    <div className={`app w-full h-screen max-h-screen `}>
      <AppHeader />
      <div className="main">
        <AppContent />
      </div>
    </div>
  );
};
export default DefaultLayout;
