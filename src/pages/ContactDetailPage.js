import { useParams } from "react-router-dom";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { contactActions } from "../slices/contact";
import ContactNotFound from "./contact/ContactNotFound";
import ContactDetail from "./contact/ContactDetail";
const ContactDetailPage = (props) => {
  const useParam = useParams();
  const dispatch = useDispatch();
  const filteredContacts = useSelector(
    (state) => state.contacts.filteredContacts
  );
  const contact = useSelector((state) => state.contact.contact);
  const error = useSelector((state) => state.contact.error);

  useEffect(() => {
    const userId = useParam.userId;
    const contact = filteredContacts.filter(
      (contact) => contact.userId === userId
    );
    if (contact && contact.length > 0) {
      dispatch(contactActions.setContact(contact[0]));
    } else {
      dispatch(contactActions.setError("Can not found any contact."));
    }
  }, [filteredContacts, useParam.userId, dispatch]);

  return (
    <div className={`profile max-h-screen h-screen mx-4 py-4`}>
      {error && error.length > 0 && <ContactNotFound error={error} />}
      {contact && <ContactDetail contact={contact} />}
    </div>
  );
};
export default ContactDetailPage;
