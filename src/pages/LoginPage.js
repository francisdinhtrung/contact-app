import Card from "../components/Card";

const LoginPage = () => {
  return (
    <div className={`flex flex-col items-center justify-center mx-4`}>
      <Card className={`flex flex-col items-center mx-auto`}>
        <h1>Login Page</h1>
        <span>Not required to implement.</span>
      </Card>
    </div>
  );
};
export default LoginPage;
