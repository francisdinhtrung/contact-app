import ContactItem from "./ContactItem";
import { useNavigate } from "react-router-dom";

const ContactList = ({ contacts }) => {
  const navigate = useNavigate();
  const handleContactClick = (contact) => {
    console.log("click: ", contact.userId);
    navigate(`/contact-detail/${contact.userId}`);
  };

  return (
    <div className={`px-8 py-4 w-full`}>
      <ul className={`space-y-2`}>
        {contacts.map((contact) => (
          <ContactItem
            key={contact.userId}
            avatar={contact.avatar}
            email={contact.email}
            fullName={contact.userName}
            phone={contact.phone}
            age={contact.age}
            gender={contact.gender}
            onContactClick={() => handleContactClick(contact)}
          />
        ))}
      </ul>
    </div>
  );
};
export default ContactList;
