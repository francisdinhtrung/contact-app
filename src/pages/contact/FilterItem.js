const FilterItem = ({ isSelected, item, onSelected }) => {
  return (
    <li className={`cursor-pointer`} onClick={onSelected}>
      <span
        className={`block rounded-3xl bg-red-500 px-4 py-2  ${
          isSelected && "active-tag"
        }`}
      >
        {item}
      </span>
    </li>
  );
};
export default FilterItem;
