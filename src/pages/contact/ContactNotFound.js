import Card from "../../components/Card";

const ContactNotFound = ({ error }) => {
  return (
    <Card
      className={`w-full text-2xl space-y-4 flex flex-col justify-center items-center`}
    >
      <h1 className={`text-4xl text-red-500`}>Resource not found!</h1>
      <span>{error}</span>
    </Card>
  );
};
export default ContactNotFound;
