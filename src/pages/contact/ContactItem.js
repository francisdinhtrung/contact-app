import { MdKeyboardArrowRight } from "react-icons/md";
import Card from "../../components/Card";
const ContactItem = ({
  avatar,
  fullName,
  phone,
  email,
  age,
  gender,
  onContactClick,
}) => {
  return (
    <Card onClick={onContactClick}>
      <li
        className={`flex flex-row justify-between items-center cursor-pointer`}
      >
        <div className={`flex flex-row space-x-4`}>
          <div className={`avatar overflow-hidden`}>
            <img
              className={`w-16 h-16 rounded-full ring-2 ring-white`}
              src={avatar}
              alt={avatar}
            />
          </div>
          <div className={`content flex flex-col`}>
            <div className={`flex flex-row space-x-4`}>
              <span className={`font-bold`}>DisplayName:</span>
              <span>{fullName}</span>
            </div>
            <div className={`flex flex-row space-x-4`}>
              <span className={`font-bold`}>Email:</span>
              <span>{email}</span>
            </div>
            <div className={`flex flex-row space-x-4`}>
              <span className={`font-bold`}>Phone:</span>
              <span>{phone}</span>
            </div>
            <div className={`flex flex-row space-x-4`}>
              <span className={`font-bold`}>Age:</span>
              <span>{age}</span>
            </div>
            <div className={`flex flex-row space-x-4`}>
              <span className={`font-bold`}>Gender:</span>
              <span>{gender}</span>
            </div>
          </div>
        </div>

        <div>
          <button className={`bg-transparent`}>
            <MdKeyboardArrowRight size={20} color={"black"} />
          </button>
        </div>
      </li>
    </Card>
  );
};
export default ContactItem;
