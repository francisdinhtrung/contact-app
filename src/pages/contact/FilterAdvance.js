import { CLEAR, filterItems, SET_FILTER } from "../../constants/constant";
import FilterItem from "./FilterItem";
import React, { useReducer } from "react";

const initialState = {
  filter: CLEAR,
  items: filterItems,
};

const filterReducer = (state, action) => {
  switch (action.type) {
    case SET_FILTER:
      return { ...state, filter: action.payload };
    default:
      return state;
  }
};
const FilterAdvance = (props) => {
  const [filterTextState, dispatchFilter] = useReducer(
    filterReducer,
    initialState
  );
  const handleSelectedFilter = (selected) => {
    const { items } = filterTextState;

    items.forEach((t) => {
      t.selected = t.typ === selected.typ;
    });
    console.log(items);
    dispatchFilter({ type: SET_FILTER, payload: selected.typ });

    props.onFilterBySelected(selected.typ);
  };

  const onClearFilter = () => {
    filterTextState.items.forEach((t) => {
      t.selected = false;
    });
    dispatchFilter({ type: SET_FILTER, payload: CLEAR });
    props.onFilterBySelected(CLEAR);
  };

  return (
    <div className={`w-full h-fit p-3 text-white flex space-x-4`}>
      <div className={`h-full items-center justify-center`}>
        <button
          onClick={onClearFilter}
          className={`bg-black rounded-full px-4 py-2`}
        >
          Clear Filter
        </button>
      </div>
      <ul className={`flex flex-row justify-center items-center space-x-4`}>
        {filterTextState.items.map((item) => (
          <FilterItem
            key={item.typ}
            isSelected={item.selected}
            item={item.typ}
            onSelected={() => handleSelectedFilter(item)}
          />
        ))}
      </ul>
    </div>
  );
};

export default FilterAdvance;
