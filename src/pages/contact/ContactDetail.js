import Card from "../../components/Card";

const ContactDetail = ({ contact }) => {
  return (
    <Card
      className={`flex flex-col space-x-4 items-center md:flex-row md:justify-start md:items-start`}
    >
      <div className={`w-[25%] space-y-8 my-4 flex flex-col items-center`}>
        <div className={`logo`}>
          <img
            className={`w-16 h-16 rounded-full ring-2 ring-white`}
            src={contact.avatar}
            alt={contact.avatar}
          />
        </div>
        <div className={`w-[70%] h-1 bg-neutral-300`}></div>
        <div className={`flex flex-col`}>
          <h1>
            <span className={`font-bold`}>DisplayName:</span> {contact.userName}
          </h1>
          <span>
            <span className={`font-bold`}>Email: </span> {contact.email}
          </span>
          <span>
            <span className={`font-bold`}>Gender: </span> {contact.gender}
          </span>
          <span>
            <span className={`font-bold`}>Phone: </span> {contact.phone}
          </span>
          <span>
            <span className={`font-bold`}>Age: </span> {contact.age}
          </span>
        </div>
      </div>

      <div className={`w-[75%]`}>
        <h1 className={`text-2xl`}>Intro</h1>
        <span>{contact.description}</span>
      </div>
    </Card>
  );
};
export default ContactDetail;
