const NotFoundPage = () => {
  return (
    <div
      className={`w-full h-screen max-h-screen flex justify-center items-center py-4 mx-auto flex flex-col`}
    >
      <h1 className={`text-red-500 font-bold text-7xl`}>404</h1>
      <span>Page Not Found!</span>
    </div>
  );
};

export default NotFoundPage;
