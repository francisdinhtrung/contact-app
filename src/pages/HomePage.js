import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { contactListActions } from "../slices/contacts";
import { AGE30, AGE65, AGELESS30, FEMALE, MALE } from "../constants/constant";

import SearchUI from "../components/Search";
import ContactList from "./contact/ContactList";
import FilterAdvance from "./contact/FilterAdvance";

const HomePage = () => {
  const dispatch = useDispatch();
  const allContacts = useSelector((state) => state.contacts.allContacts);
  const filteredContacts = useSelector(
    (state) => state.contacts.filteredContacts
  );
  const searchText = useSelector((state) => state.contacts.searchText);
  const filterText = useSelector((state) => state.contacts.filterText);

  useEffect(() => {
    if (!allContacts) {
      return;
    }

    const filteredContactLists = allContacts.filter((contact) =>
      contact.userName.toLowerCase().includes(searchText.toLowerCase())
    );

    const finalContacts = filteredContactLists.filter((contact) => {
      switch (filterText) {
        case AGE30:
          return contact.age >= 30;
        case AGELESS30:
          return contact.age < 30;
        case AGE65:
          return contact.age >= 65;
        case FEMALE:
          return contact.gender === FEMALE;
        case MALE:
          return contact.gender === MALE;
        default:
          return true;
      }
    });

    dispatch(contactListActions.setFilteredContacts(finalContacts));
  }, [allContacts, searchText, filterText, dispatch]);

  const handleSearchByKeyword = (keyword) => {
    dispatch(contactListActions.setSearchText(keyword));
  };

  const handleFilterBySelectedField = (filter) => {
    dispatch(contactListActions.setFilterText(filter));
  };

  return (
    <>
      <section className="mt-2 w-full h-72 bg-white">
        <div className="w-full h-full bg-gradient-to-r from-blue-400 to-green-500 flex items-center justify-center">
          <SearchUI onSearchHandler={handleSearchByKeyword} />
        </div>
      </section>
      <section className="w-full h-screen bg-neutral-100">
        <FilterAdvance onFilterBySelected={handleFilterBySelectedField} />
        {filteredContacts && <ContactList contacts={filteredContacts} />}
      </section>
    </>
  );
};

export default HomePage;
