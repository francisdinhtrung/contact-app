import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  allContacts: [],
  filteredContacts: [],
  searchText: "",
  filterText: ""
};
const contactListSlice = createSlice({
  name: "contactList",
  initialState: initialState,
  reducers: {
    replaceAllContacts(state, action) {
      state.allContacts = action.payload;
    },
    setFilteredContacts(state, action) {
      state.filteredContacts = action.payload;
    },
    setSearchText: (state, action) => {
      state.searchText = action.payload;
    },
    setFilterText: (state, action) => {
      state.filterText = action.payload;
    },
  },
});

export const contactListActions = contactListSlice.actions;
export default contactListSlice;
