import { faker } from "@faker-js/faker";
import { contactListActions } from "./contacts";

// use action creator thunk to
export const fetchData = () => {
  return async (dispatch) => {
    /*this is a way to represent how to fetch data by HTTP request
      but, now I will use faker lib to create set of mock data
      * */
    const minAge = 18;
    const maxAge = 80;
    const createRandomContact = () => {
      return {
        userId: faker.string.uuid(),
        userName: faker.internet.userName(),
        email: faker.internet.email(),
        phone: faker.phone.number("+84 ### ### ###"),
        avatar: faker.image.avatar(),
        registeredAt: faker.date.past({ years: 10 }).getUTCDate(),
        description: faker.lorem.paragraph(3),
        age: faker.number.int({ min: minAge, max: maxAge }),
        gender: faker.person.sex(),
      };
    };
    const fakeData = async () => {
      return faker.helpers.multiple(createRandomContact, { count: 15 });
    };

    const response = await fakeData();
    dispatch(contactListActions.replaceAllContacts(response));
  };
};
