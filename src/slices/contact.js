import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  contact: null,
  error: "",
  isLoading: false,
};
const contactSlice = createSlice({
  name: "contactDetail",
  initialState: initialState,
  reducers: {
    setContact(state, action) {
      state.contact = action.payload;
      state.error = null;
      state.isLoading = false;
    },
    setError(state, action) {
      state.error = action.payload;
      state.contact = null;
      state.isLoading = false;
    },
    setLoading(state, action) {
      state.isLoading = action.isLoading;
      state.contact = null;
      state.error = null;
    },
  },
});

export const contactActions = contactSlice.actions;
export default contactSlice;
