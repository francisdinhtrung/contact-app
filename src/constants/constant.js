export const AGELESS30 = "Age < 30";
export const AGE30 = "Age >= 30";
export const AGE65 = "Age >= 65";
export const FEMALE = "female";
export const MALE = "male";
export const SET_FILTER = "SET_FILTER";

export const CLEAR = "CLEAR";
export const filterItems = [
  { typ: AGELESS30, selected: false },
  { typ: AGE30, selected: false },
  { typ: AGE65, selected: false },
  { typ: FEMALE, selected: false },
  { typ: MALE, selected: false },
];
