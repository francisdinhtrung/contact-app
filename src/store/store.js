import { combineReducers, configureStore } from "@reduxjs/toolkit";
import contactListSlice from "../slices/contacts";
import contactSlice from "../slices/contact";
const contactListReducer = contactListSlice.reducer;
const contactReducer = contactSlice.reducer;
const rootReducer = combineReducers({
  contacts: contactListReducer,
  contact: contactReducer,
});
const store = configureStore({
  reducer: rootReducer,
  devTools: true,
});
export default store;
