import { Outlet } from "react-router-dom";
import AppFooter from "./AppFooter";

const AppContent = () => {
  return (
    <div>
      <div className={`m-auto w-full my-2 overflow-auto`}>
        <Outlet />
      </div>
      <AppFooter />
    </div>
  );
};

export default AppContent;
