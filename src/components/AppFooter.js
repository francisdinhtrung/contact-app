const AppFooter = () => {
  return (
    <section
      className={`flex flex-col w-full h-64 justify-center items-center bg-blue-900 text-white`}
    >
      <h1>Contact App</h1>
      <h1>Candidate: Trung Vu</h1>
    </section>
  );
};

export default AppFooter;
