const Card = (props) => {
  return (
    <div
      {...props}
      className={`w-full p-4 bg-white rounded-xl shadow-md ${props.className}`}
    >
      {props.children}
    </div>
  );
};
export default Card;
