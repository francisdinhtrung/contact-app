import logo from "../logo.svg";
import { NavLink } from "react-router-dom";
import { AiTwotoneLock } from "react-icons/ai";
const AppHeader = () => {
  return (
    <div
      className={`header w-full h-[80px] px-4 py-2 flex text-xl text-slate-900 shadow-xl justify-between items-center`}
    >
      <div className={`logo w-32 h-auto object-cover cursor-pointer`}>
        <NavLink to="/">
          <img src={logo} alt={`logo`} />
        </NavLink>
      </div>
      <div className={`flex flex-row items-center justify-end`}>
        <ul className={`space-x-2.5 flex flex-row`}>
          <NavLink
            className={`px-2 py-1 hover:border-b-4 hover:border-b-red-500 duration-100 cursor-pointer`}
            to="/"
          >
            Home
          </NavLink>
          <NavLink
            className={`px-2 py-1 rounded-2xl bg-red-500 text-white flex flex-row justify-between items-center gap-2
                    hover:bg-black hover:text-white`}
            to="/login"
          >
            <AiTwotoneLock size={20} />
            Login
          </NavLink>
        </ul>
      </div>
    </div>
  );
};
export default AppHeader;
