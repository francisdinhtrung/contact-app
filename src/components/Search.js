import { AiOutlineSearch } from "react-icons/ai";
import { useState } from "react";

const SearchUI = (props) => {
  const [keyword, setKeyword] = useState("");

  const onHandleKeywordChange = (event) => {
    setKeyword(event.target.value);
  };

  const submitHandler = (event) => {
    event.preventDefault();
    props.onSearchHandler(keyword);
  };

  return (
    <div className={`w-96 h-16`}>
      <form
        onSubmit={submitHandler}
        className="flex flex-row pl-4 bg-white rounded-xl justify-between items-center space-x-2"
      >
        <input
          className="w-[75%] bg-transparent outline-none"
          type="text"
          name="search"
          id="search"
          placeholder="search by name..."
          onChange={onHandleKeywordChange}
        />
        <button
          type="submit"
          className="p-3 text-white bg-red-500 rounded-xl space-x-3 flex flex-row items-center"
        >
          <AiOutlineSearch size={20} />
          Search
        </button>
      </form>
    </div>
  );
};
export default SearchUI;
